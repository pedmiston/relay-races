import os
from pathlib import Path
import requests
import pandas
import invoke

data_dir = Path('data-raw')

@invoke.task
def install(ctx, use_data=False, only_data=False):
    if use_data or only_data:
        ctx.run('./use-data.R', echo=True)
    if only_data:
        return
    ctx.run('Rscript -e "devtools::install()"', echo=True)

@invoke.task
def download_race_times(ctx):
    """Download the tables of race times from Wikipedia articles on races.""" 
    article_names = ['100_metres', '4_x_100_metres', '400_metres']
    for article_name in article_names:
        download_tables(article_name)

def download_tables(article_name):
    """Download all tables in a Wikipedia article as csv's."""
    response = requests.get(f'https://en.wikipedia.org/wiki/{article_name}')
    html_tables = pandas.read_html(response.content.decode('utf-8'), header=0)

    article_dir = data_dir / article_name
    if not article_dir.is_dir():
        article_dir.mkdir(parents=True)

    for i, table in enumerate(html_tables):
        table_csv = article_dir / f'{article_name}_{i:02d}.csv'
        table.to_csv(table_csv, index=False)

